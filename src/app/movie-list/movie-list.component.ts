import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { ApiService } from '../api.service';
@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent implements OnInit {
  searchMovieName;
  dataBackup;
  movies: any[];
  trendingMovie:any[];

  constructor(private router: Router,
    private api: ApiService) {
      this.getMovies();
      this.getTrendingMovie();
  }

  ngOnInit() {
    
    this.dataBackup = this.movies;
    console.log(this.movies);
    console.log(this.trendingMovie);
  }

/*  search() {
    this.movies = this.dataBackup.filter(movie => {
      return movie.name.toLocaleLowerCase().match(this.searchMovieName.toLocaleLowerCase());
    });

  }*/
  movieId(movieuid: number) {
    this.router.navigate(['/movie-details', movieuid])
  }
  getMovies = () =>{
    this.api.getAllMovies().subscribe(
      data => {
        this.movies = data;
      }
      
    )
    
  }
  getTrendingMovie = () =>{
    this.api.getTrendingMovie().subscribe(
      data => {
        this.trendingMovie = data;
      }
    )
  }
  getSearchResult = () =>{
    this.api.getSearchResult().subscribe(
      data => {
        this.movies = data;
      }
    )
  }

}
