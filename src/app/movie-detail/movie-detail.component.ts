import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../api.service';
@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss'],
})
export class MovieDetailComponent implements OnInit {
  movies: any[];
  movie: any;
  bookmarkMovie: any[];
  isBookmarked = false;
  currentRate: number;
  rating: number;
  constructor(private route: ActivatedRoute,
    private api: ApiService) {
    this.getMovies();
  }

  ngOnInit() {

    console.log(this.route.snapshot.params['id']);

    //this.movie = this.movies[no];

    console.log(this.movies);

    

  }

  bookmark(movie) {
    
    if (!this.isBookmarked) {
      
      this.isBookmarked = true; 

      return this.api.bookmarkMovie(this.movie, this.isBookmarked).subscribe(
        data => {
          this.movie = data;

        }
      )
    }
    else {
      this.isBookmarked = false;
      return this.api.bookmarkMovie(this.movie, this.isBookmarked).subscribe(
        data => {
          this.movie = data;

        }
      )
    }

  }
  onClick(id, rating) {
    return this.api.movieRating(id, rating).subscribe(
      data => {
        this.movie = data;
      }
    )

  }
  getMovies = () => {
    this.api.getAllMovies().subscribe(
      data => {
        const movieId = this.route.snapshot.params['id'];
        this.movies = data.filter(movie => {
          if (movie.id == movieId) {

            return movie;
          }

        }

        )
        this.movie = this.movies[0];
        this.isBookmarked = this.movie.isBookmark;
        console.log(this.isBookmarked);
      }
    )
  }

}
