import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { MovieListComponent } from './movie-list/movie-list.component';
import { BookMarkMoviesComponent } from './book-mark-movies/book-mark-movies.component';


const routes: Routes = [
  {path: 'movie-details/:id' , component: MovieDetailComponent },
  {path: '',component: MovieListComponent},
  {path: 'book-mark-movies', component: BookMarkMoviesComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
