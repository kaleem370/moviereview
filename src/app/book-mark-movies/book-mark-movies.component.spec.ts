import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookMarkMoviesComponent } from './book-mark-movies.component';

describe('BookMarkMoviesComponent', () => {
  let component: BookMarkMoviesComponent;
  let fixture: ComponentFixture<BookMarkMoviesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookMarkMoviesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookMarkMoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
