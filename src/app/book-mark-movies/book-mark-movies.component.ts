import { Component, OnInit } from '@angular/core';

import { ApiService } from '../api.service';
@Component({
  selector: 'app-book-mark-movies',
  templateUrl: './book-mark-movies.component.html',
  styleUrls: ['./book-mark-movies.component.scss']
})
export class BookMarkMoviesComponent implements OnInit {
  movies: any[];
  constructor(
    private api: ApiService
  ) {
    this.getMovies();
    
  }

  ngOnInit() {

  }
  getMovies = () => {
    this.api.getAllMovies().subscribe(
      data => {
      /*data.filter(movie=>{
        if (movie.isBookmark == true){
          return movie
        }*/
        
       this.movies =  data.filter( movie =>{
          if (movie.isBookmark == true ){
            return movie;
          }
    
        }

        )
      
      }
      
    )
  }



}
