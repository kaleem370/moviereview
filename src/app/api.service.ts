import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ReturnStatement } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseurl  = "http://127.0.0.1:8000/";
  httpHeaders = new HttpHeaders({'content-Type':'appication/json'})

  constructor( private http:HttpClient) { }

  getAllMovies(): Observable<any>{
    return this.http.get(this.baseurl + 'api/movie/')
  }
  getTrendingMovie(): Observable<any>{
    return this.http.get(this.baseurl + 'api/trending_movie/')
  }
  getSearchResult(): Observable<any>{
    return this.http.get(this.baseurl + 'api/search/?search=')

  }
  bookmarkMovie(movie, isBookmark): Observable<any>{
    
    const bookmark = {isBookmark:isBookmark};


    console.log(movie);
    console.log(bookmark);
    return this.http.patch(this.baseurl+ 'movie/update-partial/' + movie.id + '/', bookmark)
  }
  movieRating(id, rating): Observable<any>{
    const movieRating = {rating:rating};
    return this.http.patch(this.baseurl+ 'movie/update-partial/' + id + '/', movieRating)
  }
}
